import React, { Component } from 'react';
import foto2 from '../images/foto2.png'
import {
  Container, Row, Col
} from 'reactstrap';

class DataAnalytics extends Component {

  render() {
    return (
      <Container>
        <Row>
          <Col>
          <div className="text-center m-5">
          <h1>Data Analytics</h1>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius iure magnam voluptatibus atque quae! Possimus.</p>
          </div>
          </Col>
        </Row>      
        <Row>
          <Col xs="12" sm="4">
          <div className="text-center">
            <i className="fas fa-university"></i>
            <h4>Lorem ipsum dolor sit.</h4>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Laborum temporibus beatae atque excepturi maxime facere.</p>
          </div>
          </Col>
          <Col xs="12" sm="4">
          <div className="text-center">
            <i className="fas fa-money-check-alt"></i>
            <h4>Lorem ipsum dolor.</h4>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Laborum temporibus beatae atque excepturi maxime facere.</p>
          </div>
          </Col>
          <Col xs="12" sm="4">
          <div className="text-center">
            <i className="fas fa-piggy-bank"></i>
            <h4>Lorem ipsum dolor amet.</h4>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Laborum temporibus beatae atque excepturi maxime facere.</p>
          </div>
          </Col> 
        </Row>
        <Row className="p-5"> 
          <Col xs="12" md="6" className="p-5">
            <img src={foto2} alt="Foto"/>
          </Col>
          <Col xs="12" md="6">
          <h1 className="mb-4">Data Analytics</h1>
          <i className="fas fa-gift"></i>
          <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ex, voluptate distinctio. Deleniti vero, voluptatem esse, nostrum laboriosam, cupiditate id quae soluta nisi non doloribus at ipsam qui numquam omnis repellendus.</p>
          <i className="fas fa-magic"></i>
          <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ex, voluptate distinctio. Deleniti vero, voluptatem esse, nostrum laboriosam, cupiditate id quae soluta nisi non doloribus at ipsam qui numquam omnis repellendus.</p>
          <i className="fab fa-skyatlas"></i>
          <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ex, voluptate distinctio. Deleniti vero, voluptatem esse, nostrum laboriosam, cupiditate id quae soluta nisi non doloribus at ipsam qui numquam omnis repellendus.</p>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default DataAnalytics;