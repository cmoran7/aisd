import React, { Component } from 'react';
import foto from '../images/foto.png'
import {
  Container, Row, Col, Button
} from 'reactstrap';

class AppMain extends Component {
  render(){
    return (
      <Container className="pb-5">
        <Row>
          <Col xs="12" md="6" className="p-5">
          <h1>AISD TECH</h1>
          <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ex, voluptate distinctio. Deleniti vero, voluptatem esse, nostrum laboriosam, cupiditate id quae soluta nisi non doloribus at ipsam qui numquam omnis repellendus.</p>
          <Button size="lg" className="rounded-pill m-2" active>Get app now</Button>{' '}
          <Button size="lg" className="rounded-pill m-2">Get app now</Button>
          </Col>
          <Col xs="12" md="6">            
            <img src={foto} alt="Foto"/>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default AppMain;