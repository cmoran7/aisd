import React, { Component } from 'react';
import logo from '../images/logo.png';
import {
  Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Container, Button
} from 'reactstrap';

class AppNavbar extends Component {
  state = {
    isOpen: false
  }
  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render () {
    return (
      <header>
        <Navbar expand="sm" className="mb-5">
          <Container>
            <NavbarBrand href="/"><img src={logo} alt="Logo" /></NavbarBrand>
            <NavbarToggler className="navbar-dark" onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink className="text-white" href="/">Home</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="text-white" href="/">Features</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="text-white" href="/">Videos</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="text-white" href="/">Pricing</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="text-white" href="/">Review</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="text-white" href="/">Downloads</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="text-white" href="/">Blog</NavLink>
                </NavItem>
                <NavItem>
                  <Button color="success" className="rounded-pill">Get Started</Button>
                </NavItem>
              </Nav>
            </Collapse>
          </Container>
        </Navbar>
      </header>
    );
  }
}

export default AppNavbar;
