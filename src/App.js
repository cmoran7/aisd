import React, { Component } from 'react';
import AppNavbar from './components/AppNavbar';
import AppMain from './components/AppMain';
import DataAnalytics from './components/DataAnalytics';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <div className="App">
          <AppNavbar />
          <AppMain />
        </div>
        <div>
          <DataAnalytics />
        </div>
      </div>
    );
  }
}

export default App;
